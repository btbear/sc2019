"""M345SC Homework 1, part 1
Daniel Trent 01051314
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion:
    
    To search for frequently occuring k-mers in a DNA sequence of length N requires that
    we keep track of the k-mers which we have already found, and that we have a quick way
    to compare k-mers. The best way to do this is with a hash table - here we will use
    Python's in-built dictionary. This performs well, in the sense that comparison is not
    affected by the size of k, and that it scales well with N since dictionary lookup
    is not affected by the number of items in it. 
    
    The algorithm runs as follows:
    
    1) Setup - initialise 3 lists and a dictionary and get length of sequence.
    2) For each index in S extract the k-mer starting from that index.
       If the k-mer is new, add it's location to the dictionary else
       add the location to the list belonging to the k-mer.
    3) For each key in the dictionary, check the length of the list of locations.
       If it is longer than 'f' add the string to L1 and add the list of locations to L2.
    4) For each frequent k-mer in L1, append the negative of the number of appearences to L3.
       Mutate the string at index 'x' to each of A,C,G and T. 
       For each mutation, if the string is in the dictionary add the number of appearences to L3.
       
    Note that at step 4), at least one of the strings tried - that is the original k-mer -
    will be in the dictionary, so every element of L3 will be at least 0.
    
    Analysis of asymptotic complexity:
        
    1) Each operation in setup requires O(1) time to complete and we have 5 operations.
    
    2) Since we are looking for k-mers, step 2) iterates over N-k+1 starting indeces in S.
       For each index we extract the k-mer as a slice of the string. This is done in O(k) time
       since python makes a copy of the string which involves k assignments.
       We then check if the string is already in the Dictionary. This is done in O(1) time since 
       the dictionary is akin to a hash table. Then we have an assignment and an increment of the 
       index, both of which are O(1).
       For step 2) we have O((k+3)*(N-k+1)) time complexity.
    
    3) Given four bases for DNA there are 4**k possible k-mers.
       However there are at most N-k+1 different k-mers contained in S.
       So the loop over the dictionary is  at most min(4**k,N-k+1).
       When k is small and N is large, this is 4**k. For larger k, this is N-k+1.
       For each loop in the dictionary we find the length of a list which is O(1).
       We then have a comparison. Each k-mer is either frequent or not, but there can be 
       at most min(4**k,(N-k+1)/f) frequent k-mers in S.
       For each frequent k-mer there are 2 assignments.
       We then have an increment of the index.
       For step 3) we have at most 2*min(4**k,N-k+1) + 2*min(4**k,(N-k+1)/f) operations.
       
    4) The list of frequent k-mers has at most min(4**k,(N-k+1)/f) items.
       For each item we have a dictinary lookup, find length and assignment all of which
       are order 1. We then have two slices which together have length k-1 and two 
       assignments giving k+1 operations. We also have an index increment.
       To find x-point mutations, we take advantage of the fact that there are only 4
       bases, and use the dictionary to search for the 4 possible x-point mutations of 
       a k-mer.
       Each mutation requires 2 string concatenations, a dictionary lookup and an increment
       of the index. If the lookup is positive we have another dictionary lookup, find length
       and assignment.
       So we have at most (k+1+4 + 4*7)*min(4**k,(N-k+1)/f) = (k+33)*min(4**k,(N-k+1)/f) operations.
       
    Putting all of this together we have:
    (k+3)*(N-k+1) + 2*min(4**k,N-k+1)  + (k+35)*min(4**k,(N-k+1)/f) + 5
    operations.
    The dominant term is k(N-k+1) which is dependent on the sizes of N and k.
    When k is small in comparison to N the algorithm has approximate O(N) complexity.
    When k is close in size to N, the term (N-k+1) is small, so the algorithm has O(k) complexity
    which is again similar to O(N).
    When k is close to N/2 we have O(N^2) complexity which is the worst case scenario.
    
    
    Real world performance:
    
    The asymptotic analysis assumes the worst case at all times. However it is worth noting
    that as k get larger the probability of a particalar k-mer being found is 1/4**k. This
    means that k does not have to be very large in relation to N before the list of 
    frequent k-mers becomes much shorter than the theoretical (N-k+1)/f maximum, which means
    that step 4 of the algorithm is much faster than the analysis suggests.
    
    It is also worth noting that string slicing in python is much faster than a similar length 
    for loop, at least for strings of reasonable length, despite the theoretical O(k) complexity,
    due to the fast C implementation. However for very long slices, memory constraints come 
    into play, which slows the performance.
    """

    # Part 1: Setup
    N = len(S) #get length of string
    assert k < N
    assert x <=k
    L1,L2,L3=[],[],[] #initialise lists to return
    
    # initialise dictionaries
    locDict = {} # holds locations of k-mers
    
    # Part 2
    # Iterate over characters in the string
    for ind in range(N-k+1):
        string = S[ind:ind+k] # extract k-mer beggining at index
        if string in locDict: # check if k-mer already found
            locDict[string].append(ind) # add index to list
        else: # if new k-mer
            locDict[string] = [ind] # add new k-mer and location
    
    # Part 3
    for s,n in locDict.items(): # iterate over dictionary of k-mers
        if len(n)>=f: # if list of locations is longer than f
            L1.append(s) # add frequent k-mer to L1
            L2.append(locDict[s]) # add list of locations to L2
    
            
    # Part 4        
    for i in L1: # iteratate over list of frequent k-mers
        L3.append(-len(locDict[i])) # subtract # of times base k-mer appears
        a = i[:x] # part of string up to point x
        b = i[x+1:] # part of string after point x
        for j in ["A","C","G","T"]: #iterate over possible values at x
            if a+j+b in locDict: # if mutated k-mer present
                L3[-1] += len(locDict[a+j+b]) #add # of appearences to list
                
    return L1,L2,L3




if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    from time import time
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    k=3
    x=2
    f=2
    print(L1)
    print(L2)
    print(L3)
    
