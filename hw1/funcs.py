# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 10:39:24 2019

@author: dantr
"""

def lowerbound(L,x,low,high):
    if low > high:
        return low
        
    mid = low + (high-low)//2
        
    if L[mid]>=x:
        return lowerbound(L,x,low,mid-1)
    else:
        return lowerbound(L,x,mid+1,high)
        
def upperbound(L,x,low,high):
    if low > high:
        return low
        
    mid = low + (high-low)//2
        
    if L[mid]>x:
        return upperbound(L,x,low,mid-1)
    else:
        return upperbound(L,x,mid+1,high)