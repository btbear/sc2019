"""M345SC Homework 1, part 2
Daniel Trent 01051314
"""

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """
    # Setup
    Lout=[]
    N = len(L) # number of sublists
    M = len(L[0]) # length of lists
    MmP = M-P-1 # length of unordered section of sublists
    
    
    def lowerbound(L,x,low,high):
        """
        Finds the lower bound on the indeces of matches in an ordered list
        between indeces 'low' and 'high', by performing a recursive middle split
        on the sublist. If no match found will return 'high'.
        
        Input:
        L: sorted list
        x: target
        low: starting index
        high: end index
        
        Output: index such that L[index]==x and L[index-1]!= x
        """
        if low > high:
            return low
        
        mid = low + (high-low)//2 # find mid point
        
        if L[mid]>=x: # if midpoint >= target
            return lowerbound(L,x,low,mid-1) # find lower bound below mid
        else:
            return lowerbound(L,x,mid+1,high) # find lower bound above mid
        
    def upperbound(L,x,low,high):
        """
        Finds the upper bound on the indeces of matches in an ordered list
        between indeces 'low' and 'high', by performing a recursive middle split
        on the sublist. If no match found will return 'high'.
        
        Input:
        L: sorted list
        x: target
        low: starting index
        high: end index
        
        Output: index such that L[index-1]==x and L[index]!= x
        """
        if low > high:
            return low
        
        mid = low + (high-low)//2
        
        if L[mid]>x:
            return upperbound(L,x,low,mid-1)
        else:
            return upperbound(L,x,mid+1,high)
    
    def multiple(L,P,x,Lout,subl):
        """
        Appends matches from sorted list to Lout
        """
        lb = lowerbound(L,x,0,P-1) # lower bound
        ub = upperbound(L,x,0,P-1) # upper bound
        for i in range(lb,ub): # for indeces between bounds
            Lout.append([subl,i]) # append to Lout
        return Lout
    

    
    # standard linear search 
    def linearsearch(L,x,Lout, subl,MmP,P):
        """
        Performs a standard linearch search on a list which starts from a non
        zero index.
        Inputs:
        L: list
        x: target to match
        Lout: list of lists of match locations
        subl: index of sublist in original L
        MmP: length of list
        P: starting index
        
        Returns modified Lout.
        """
        for ind in range(MmP+1):
            if L[ind] == x: # check match
                Lout.append([subl,ind+P]) # append to list
        return Lout
    
    for subl in range(N): # for each sublist
        sublist = L[subl] # extract sublist
        # run multiple on sorted part of list
        Lout = multiple(sublist[:P],P,target,Lout,subl)
        # linear search on unsorted part of list
        Lout = linearsearch(sublist[P:],target,Lout,subl,MmP,P)

    
    return Lout


def nsearch_time(a=True,b=True,c=True,d=True):
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Discussion: (add your discussion here)
    
    
    Algorithm:
    
    The nsearch algorithm makes use of two algorithms - 
    linear search and a modified version of binary search.
    
    The modified Binary search is used to take advantage of the sorted part of
    the list. Instead of finding one match location, this function finds the upper 
    and lower bounds of the range of values in the sorted list which match the target.
    Since the list is sorted, all the intermediate values must also match.
    The time complexity of the binary search is 2*O(log(P)), since the binary search
    is run twice for the two bounds. However the loop which appends the list of locations
    to Lout is is O(P). Nonetheless we would expect that this linear part of the algorithm
    be short in comparison to the length of the list, giving the algorithm a time advantage 
    over the standard linear search. It also uses only one assignment operation,
    instead of multiple comparisons and assignments.
    
    Linear search is used to find matches in the unsorted part.  
    The time complexity of this linear search is O(M-P).
    
    Both algorithms are run on the N lists contained in L.
    The complexity is therefore in the order of N*(P + 2*log(P) + M-P), which gives
    O(N*M) for the asymptotic complexity.
    
    
    Performance:
    
    To test the performance of the algorithm and dependency on inputs, we take an
    average of a large number of runs, changing one parameter while keeping the others
    constant, in particular we have P = M/2 when changing M or N.
    Figure 1 shows the dependence on M and N spearately and they both show a linear
    trend as expected.
    The green line illustrates the dependence when N=M and they are increased together
    and shows a quadratic shape, which affirms that the algorithm is O(N*M) and not
    O(N+M).    
    
    Figure 2 shows that the relative performance of this algorithm is dependent on
    the size of the ordered part of the lists, P. In particular, the running time
    decreases approximately linearly as P increases relative to M, showing that the
    algorithm for the sortedpart of the list is indeed faster than a linear search,
    and so the algorithm as a whole approaches N*log(P) time as P approaches M.
    
    
    """
    from time import time
    import numpy as np
    import matplotlib.pyplot as plt
    if a:
        # Dependency on N
        M = 1000 # set M constant
        nTime = [0]*12 # initalise list to hold times
        for n in range(1,13): # range of N
            for itr in range(50): # repeat simulation 100 times
                # generate an array of random integers of size(N=2**n,M)
                l = np.random.randint(10^4,size=[2**n,M])
                P = M//2 # set P constant M/2
                # sort first P elements of each row and convert to list of lists
                L = np.append(np.sort(l[:,0:P]) , l[:,P:],axis=1).tolist()
                # set target allowing out-of-range values
                target = np.random.randint(10^4 + 10)
                t1 = time()
                y = nsearch(L,P,target) # run algorithm
                t2 = time()
                nTime[n-1] += t2-t1 # add to total time
            #print(nTime)
        # plot average time vs N
        plt.plot(2**np.linspace(1,12,12),np.array(nTime)/50)
        plt.xlabel("x")
        plt.ylabel("Average Time")
        plt.title("Daniel Trent-01051314-nsearch \n Dependency on M and N")
    if b:
        # Dependency on M
        N = 1000 #set N constant
        nTime = [0]*12 # initalise list to hold times
        for m in range(1,13): # range of M
            for itr in range(50): # repeat simulation 100 times
                # generate an array of random integers of size(N,M=2**m)
                l = np.random.randint(10^4,size=[N,2**m])
                P = 2**(m-1) # set P constant M/2
                # sort first P elements of each row and convert to list of lists
                L = np.append(np.sort(l[:,0:P]) , l[:,P:],axis=1).tolist()
                # set target allowing out-of-range values
                target = np.random.randint(10^4 + 10)
                t1 = time()
                y = nsearch(L,P,target) # run algorithm
                t2 = time()
                nTime[m-1] += t2-t1 # add to total time
            #print(nTime)
        #plt.figure()
        # plot average time vs M
        plt.plot(2**np.linspace(1,12,12),np.array(nTime)/50)
        #plt.xlabel("M")
        #plt.ylabel("Average Time")
        #plt.title("Average running time vs Length of lists \n (using 1000 lists)")     
    if c:
        # Dependency on N*M
        nTime = [0]*12 # initalise list to hold times
        for m in range(1,13): # range of N=M
            for itr in range(50): # repeate simulation 50 times
                # generate an array of random integers of size(N=2**m,M=2**m)
                l = np.random.randint(10^4,size=[2**m,2**m])
                P = 2**(m-1) # set P constant M/2
                # sort first P elements of each row and convert to list of lists
                L = np.append(np.sort(l[:,0:P]) , l[:,P:],axis=1).tolist()
                # set target allowing out-of-range values
                target = np.random.randint(10^4 +10)
                t1 = time()
                y = nsearch(L,P,target) # run algorithm
                t2 = time()
                nTime[m-1] += t2-t1 # add to total time
            #print(nTime)
        #plt.figure()
        # plot average time vs N=M
        plt.plot(2**np.linspace(1,12,12),np.array(nTime)/50)
        #plt.xlabel("N,M")
        #plt.ylabel("Average Time")
        #plt.title("Average running time")
        plt.legend(["N=x,M=1000","M=x,N=1000","N=M=x"])
    if d:
        # Dependency on P
        nTime = [0]*11 # initalise list to hold times
        for p in range(11): #range of P
            print(p)
            for itr in range(50): # repeat simulation 50 times
                # generate an array of random integers of constant size(N=2**10,M=2**10)
                l = np.random.randint(10^4,size=[2**10,2**10])
                P = p*(2**10)//10 # set P
                # sort first P elements of each row and convert to list of lists
                L = np.append(np.sort(l[:,0:P]) , l[:,P:],axis=1).tolist()
                # set target allowing out-of-range values
                target = np.random.randint(10^4 +10)
                t1 = time()
                y = nsearch(L,P,target) # run algorithm
                t2 = time()
                nTime[p] += t2-t1 # add to total time
            #print(nTime)
        plt.figure() # new figure
        # plot average time vs ratio of P to M
        plt.plot((np.linspace(0,10,11))/10,np.array(nTime)/50)
        plt.xlabel("P/M")
        plt.ylabel("Average Time")
        plt.title("Daniel Trent-01051314-nsearch \n Dependency on P")

    return None #Modify as needed


if __name__ == '__main__':

    #add call(s) to nsearch here which generate the figure(s)
    #you are submitting
    nsearch_time() 
    #modify as needed
    