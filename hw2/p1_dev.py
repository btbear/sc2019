"""M345SC Homework 2, part 1
Daniel Trent 01051314
"""

def scheduler(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list my also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    S: A list of integers corresponding to the schedule of tasks. S[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Discussion:
    
    Consider the tasks as nodes in a graph. Then dependencies are directed edges
    such that i <- j if i is a depedency for j.
    We can then use a modified version of depth first search (DFS) to find the order of 
    dependencies. In particular, the day of task i is 1 more than the day of its 
    largest dependent. This is equivalent to the depth of the tree produced by DFS,
    since any connected nodes must be dependents. 
    Since we do not wish to compute a new tree for each individual node, we can compute
    the day for each node recursively by setting the day of each node to be the 
    maximum plus 1 of the respective days of all its dependent nodes. This allows us
    to compute days for each node as we go along, so each node is only explored once.
    The DFS algorithm relies on having only one connected component, so the algorithm
    will need to be applied to all nodes, to account for multiple possible components.
    
    The recursive algorithm runs as follows:
        Algorithm Compute day:
        1. IF Node is explored: DO nothing
        2. ELSE: Set Node as explored
        3.    IF Node has no dependencies:
        4.        Set node day to 0
        5.    ELSE: max <- 0
        6.        FOR each dependent node v:
        7.            IF v is unexplored:
        8.                Compute day for v
        9.            IF day > max:
        10.              max <- day
        11. RETURN max + 1
        
    For this function each node is explored only once. Each edge is encountered once
    (as they are directed). The asysmptotic complextity is therefore O(N+D) where D
    is the number of edges/dependencies. D can be considered a multiple of M where M
    is the number of nodes that have dependencies so the complexity is O(N+M) which
    is expected from a DFS-like method.    
        
    
    """

    
    def sch_helper(L,node,Lexplore,S):
        """" 
        Helper function for scheduler. Provides a recursive method for 
        computing the schedule day for 'node' and all its dependencies.
        
        Input:
            L: Dependency list
            node: starter node
            Lexplore: List of flags for nodes explored
            S: List of schedule days
        Output:
            S, Lexplore - modified
        """
        if Lexplore[node]==1: # if explored
            return S,Lexplore # do nothing
        Lexplore[node]=1 # else set node as explored
        if L[node] == []: # Base case - no dependencies
            S[node] = 0 # schedule day 0
        else: # if depencies
            maxm = 0 # initialise max
            for v in L[node]: # for each dependent task
                if Lexplore[v] == 0: # if unexplored
                    S,Lexplore = sch_helper(L,v,Lexplore,S) # recursive call to explore
                # set max to be the larger of current or new node day
                maxm = max([maxm,S[v]]) 
                
            S[node] = maxm+1 # set day of node to be one more than largest dependency
        return S,Lexplore # return lists that have been modified
    
    ###### Main Function##########
    N = len(L)
    Lexplore = [0]*N #initialise list of explored flags
    S=[0]*N # initialse list of schedule days
    for i in range(N): # iterate over all nodes
        S,Lexplore = sch_helper(L,i,Lexplore,S) # update lists
    return S


def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal
    from node J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2 from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Discussion: Add analysis here
    
    Since the signal is boosted back to a0 at each junction a path is valid if it
    contains no edges ij such that Lij < amin/a0.
    Thus this problem can be reduced to finding a path to J2 through an unweighted
    network where edges with Lij < amin/a0 have been removed. This can be accomplished
    with a standard breadth first search (BFS) adjusted to ignore edges with low weight.
    
    
    """
    from collections import deque
    
    minval = amin/a0 # set minimum weight
    
    Lexp = [0 for l in A] #flags

    L = [[] for l in A] #paths
    
    Q = deque() # initialise queue
    Q.append(J1) # start queue with J1
    Lexp[J1]=1 #set J1 as explored
    
    L[J1]=[J1] # start path
    while len(Q)>0:
        x = Q.popleft() #remove node from front of queue       
        for v,l in A[x]:
            if (Lexp[v]==0)&(l>=minval): # if unexplored and weight is sufficient
                Q.append(v) #add unexplored neighbors to back of queue
                Lexp[v]=1 # set node as explored
                L[v].extend(L[x]) #Add path to node x and node v to path
                L[v].append(v)     #for node v    
    
    return L[J2]


def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency list, A)

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Discussion: 
        
    This problem amounts to finding the smallest weight 'a' on ALL paths from J1 to J2.
    The largest of these 'ahigh' gives the path and a0min such that a0min = amin/ahigh.
    
    The algorithm for finding a0min is as follows:
        
    Maintain a queue of paths and lowest weight in each path.
    Start a path at J1 with a large weight.
    Out <- (output weight,[output path])
    WHILE queue is not empty:
        Remove the top path from the queue.
        IF the path ends at the target.
            IF lowest weight larger than output weight
                Out <- (current weight, current path)
        ELSE FOR each node connected to the last node in the path
            IF node is not already in path
                create a new path of the current path with new node added
                IF new weight smaller than path weight
                    new path weight <- new weight
                add new path to queue
    
    Asymptotic complexity:
    There will be a path created for each edge on each node
    
    """
    from collections import deque
    
  

    L4 = [-1,[]] #path
    
    Q = deque()
    path = [1,[J1]]
    Q.append(path)

    while len(Q)>0:
        path = Q.popleft() #remove path from front of queue
        x = path[1][-1]
        if x==J2:
            if path[0]>L4[0]:
                L4 = path
        else:
            for v,l in A[x]:
                if v not in path[1]:
                    newpath = [path[0],path[1].copy()]
                    (newpath[1]).append(v) #add unexplored neighbors to back of queue
                    newpath[0] = min([newpath[0],l])
                    Q.append(newpath)

     
    
    output = amin/L4[0],L4[1] #Modify as needed

    return output


if __name__=='__main__':
    #add code here if/as desired
    w=None #modify as needed
