"""M345SC Homework 2, part 2
Your name and CID here
"""
import numpy as np
import networkx as nx
import scipy as sp
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    y0 = (0.05,0.05,0.1)
    #S = np.zeros(Nt+1)

    #Add code here
    #I = np.zeros(Nt+1)
    #V = np.zeros(Nt+1)
    
    def RHS(siv,t):
        theta = theta0 + theta1*(1-np.sin(2*np.pi*t))
        s = a*siv[1] - (g+k)*siv[0]
        i = theta*siv[0]*siv[2]  - (k+a)*siv[1]
        v = k*(1-siv[2]) - theta*siv[0]*siv[2]
        return [s,i,v]
    
    y = odeint(RHS,y0,tarray)
    if display:
        plt.figure()
        plt.plot(tarray,y[:,0])
        #plt.plot(tarray,y[:,1])
        #plt.plot(tarray,y[:,2])
        #plt.plot(tarray,np.sum(y,axis = 1))
        #plt.legend(["s","i","v"])
        plt.xlabel('t')
        plt.ylabel('S(t)')
    return y[:,0]

def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
                each time step.
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    Smean = np.zeros(Nt+1)
    Svar = np.zeros(Nt+1)

    #Add code here
    N = nx.number_of_nodes(G)
    s0 = np.zeros(N)
    i0 = np.zeros(N)
    v0 = np.ones(N)
    s0[x] = 0.1
    i0[x] = 0.05
    v0[x] = 0.05
    y0 = np.concatenate([s0,i0,v0])
    
    A = nx.adjacency_matrix(G) # adjacency matrix - scipy.sparse.csr
    # contruction of the Flux matrix
    q = np.array([y for x,y in G.degree()]) # vector of degree
    qDiag = sp.sparse.diags(q) # diagonal matrix of degree
    num = qDiag*A # 'numerator' of F ### '*' operator is overloaded
    denom = sp.sparse.diags(1/(A*q)) #'denominator' of F as sparse diag
    F = tau*num*denom # F 
    
    
    def RHS(y,t):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Discussion:
        
        The number of operations for RHS is 18N + 24M + 9. The complexity is then
        O(N+M), where the graph has N nodes and M edges. y has length 3N.
        
        This calculation is as follows.
        
        1. Slicing y into three parts takes 3*N operations 
        2. Computing theta takes 7 operations, counting sin computation as 1.
        3. Computing sv takes 2*N for N additions and assignments
        
        To compute ds,di and dv there are six types of operations
        1) scalar*vector, 2) matrix*vector, 3) matrix*matrix, 4) sum of matrix rows,
        5) scalar + vector, 6) scalar + scalar.
    
        1) The number of operations for scalar*vector is N.
        2) Since the matrices are in sparse format, the number of operations
            for matrix-vector multiplication is related to the number of non-zero entries.
            For each non-zero entry we have a multiplication and addition.
            In this case the matrix is F which has 2M non-zero entries, the same as
            the adjacency matrix. So we have 4M operations.
        3) For the matrix-matrix multiplication we have a sparse matrix * a sparse diagonal
            matrix so the number of operations is 2M corresponding to multiplication of non-zero
            entries in each column by a scalar.
        4) The sum of matrix rows is also 2M due to sparse format.
        5) The number of operations for scalar + vector is N.
        6) The number of operations for scalar + scalar is 1.
    
        We also have a conversion of a vector to a sparse diagonal matrix. We will
        assume this takes N operations for assignment of values to the diagonal.
        
        4. Computing ds takes N + (1+N) + 4M + (2M+2M +N) = 3N + 8M +1 operations.
        5. Computing di takes 3N + 8M +1 operations.
        6. Computing dv takes 2N + N + 4M + (2M+2M +N) = 4N + 8M operations.
        7. Forming the final vector takes 3N operations.
        The speed of the computation is boosted significantly by having F
        precomputed.
    
        """
        S = y[:N]
        I = y[N:2*N]
        V = y[2*N:]
        
        theta = theta0 + theta1*(1-np.sin(2*np.pi*t))
        sv = S*V
        
        ds = a*I - (g+k)*S + F*S - sp.sum(F*sp.sparse.diags(S),axis = 0)
        di = theta*sv - (k+a)*I + F*I - sp.sum(F*sp.sparse.diags(I),axis = 0)
        dv = k*(1- V) - theta*sv + F*V - sp.sum(F*sp.sparse.diags(V),axis = 0)
        

        dy = np.ravel(np.concatenate([ds,di,dv]))#modify
        return dy

    y = odeint(RHS,y0,tarray)
    Smean = np.mean(y[:,:N],axis = 1)
    Svar = np.var(y[:,:N],axis = 1)
    if display:
        plt.figure()
        plt.plot(tarray,Smean)
        plt.xlabel('t')
        plt.ylabel('<S(t)>')
        
        plt.figure()
        plt.plot(tarray,Svar)
        plt.xlabel('t')
        plt.ylabel('Var(S(t))')

    return Smean,Svar


def diffusion():
    """Analyze similarities and differences
    between simplified infection model and linear diffusion on
    Barabasi-Albert networks.
    Modify input and output as needed.

    Discussion: add discussion here
    """
    
    
    
    return None #modify as needed

def diff(G,D,x = 0,Y0 = (0.1,0.05,0.05),tf=6,Nt=400,display=False):
        """ Diffusion model
        HELPER FUNCTION FOR DIFFUSION
        """
        N = nx.number_of_nodes(G)
        s0 = np.zeros(N)
        i0 = np.zeros(N)
        v0 = np.ones(N)
        s0[x] = Y0[0]
        i0[x] = Y0[1]
        v0[x] = Y0[2]
        y0 = np.concatenate([s0,i0,v0])
        
        L = nx.laplacian_matrix(G)
        tarray = np.linspace(0,tf,Nt+1)
        def RHS(y,t):
            S = y[:N]
            I = y[N:2*N]
            V = y[2*N:]
            
            ds = -D*L*S
            di = -D*L*I
            dv = -D*L*V
            
            dy = np.ravel(np.concatenate([ds,di,dv]))
            return dy
        y = odeint(RHS,y0,tarray)
        return y
    
def mdlN(G,x=0,Y0 = (0.1,0.05,0.05),params=(0,80,0,0,0,0.01),tf=6,Nt=400):
    """ Non-linear model with all params bar theta0,tau =0
    HELPER FUNCTION FOR DIFFUSION
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    
    N = nx.number_of_nodes(G)
    s0 = np.zeros(N)
    i0 = np.zeros(N)
    v0 = np.ones(N)
    s0[x] = Y0[0]
    i0[x] = Y0[1]
    v0[x] = Y0[2]
    y0 = np.concatenate([s0,i0,v0])
    
    A = nx.adjacency_matrix(G) # adjacency matrix - scipy.sparse.csr
    # contruction of the Flux matrix
    q = np.array([y for x,y in G.degree()]) # vector of degree
    qDiag = sp.sparse.diags(q) # diagonal matrix of degree
    num = qDiag*A # 'numerator' of F ### '*' operator is overloaded
    denom = sp.sparse.diags(1/(A*q)) #'denominator' of F as sparse diag
    F = tau*num*denom # Flux matrix, order of num and denom is important 
    
    
    def RHS(y,t):
        S = y[:N]
        I = y[N:2*N]
        V = y[2*N:]
        
        theta = theta0 + theta1*(1-np.sin(2*np.pi*t))
        tsv = theta*S*V
        
        ds = a*I - (g+k)*S + F*S - sp.sum(F*sp.sparse.diags(S),axis = 0)
        di = tsv - (k+a)*I + F*I - sp.sum(F*sp.sparse.diags(I),axis = 0)
        dv = k*(1- V) - tsv + F*V - sp.sum(F*sp.sparse.diags(V),axis = 0)
        
        dy = np.ravel(np.concatenate([ds,di,dv]))#modify
        return dy

    y = odeint(RHS,y0,tarray)

    return y

if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    o=None #modify as needed
