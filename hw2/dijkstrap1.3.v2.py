"""Dijkstra algorithm implemented using dictionaries.
Note: Implementation is different from lecture 8 slides but similar
to the in-class example.
"""
import networkx as nx

def dijkstra(G,s):
    """Find shortest distances to s in weighted graph, G"""

    #Initialize dictionaries
    dinit = -1#10**6
    Edict = {} #Explored nodes
    Udict = {} #Uneplroed nodes

    for n in G.nodes():
        Udict[n] = dinit
    Udict[s]=0

    #Main search
    while len(Udict)>0:
        #Find node with min d in Udict and move to Edict
        dmin = -1
        for n,w in Udict.items():
            if w>dmin:
                dmin=w
                nmin=n
        Edict[nmin] = Udict.pop(nmin)
        print("moved node", nmin)

        #Update provisional distances for unexplored neighbors of nmin
        for n,w in G.adj[nmin].items():
            if n in Udict:
                wey = w['weight']
                dcomp = min([dmin, w['weight']])
                if Udict[n]==-1:
                    if dmin == 0:
                        Udict[n]=wey
                    else:
                        Udict[n]=dcomp
                elif dcomp>Udict[n]:
                    Udict[n]=dcomp
        print(Udict)
    return Edict


if __name__=='__main__':
    #Example from lecture 8 slides
    e=[[1,2,0.5],[1,5,0.6],[2,3,0.7],[2,5,0.9],[5,4,0.3],[3,4,0.2],[4,6,0.1]]
    G = nx.Graph()
    G.add_weighted_edges_from(e)
    Edict = dijkstra(G,1)
    