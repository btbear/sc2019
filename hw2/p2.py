"""M345SC Homework 2, part 2
Daniel Trent 01051314
"""
import numpy as np
import networkx as nx
import scipy as sp
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1) #time array
    y0 = (0.05,0.05,0.1) # intial values

    def RHS(y,t): # funciton for computing gradient when tau = 0
        theta = theta0 + theta1*(1-np.sin(2*np.pi*t))
        s = a*y[1] - (g+k)*y[0]
        i = theta*y[0]*y[2]  - (k+a)*y[1]
        v = k*(1-y[2]) - theta*y[0]*y[2]
        return [s,i,v]
    
    y = odeint(RHS,y0,tarray) # time marching method
    if display: # plot
        plt.figure()
        plt.plot(tarray,y[:,0])
        plt.xlabel('t')
        plt.ylabel('S(t)')
        plt.title("S vs Time for initially infected node (tau=0)")
    return y[:,0]

def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
                each time step.
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    #Smean = np.zeros(Nt+1) #not needed
    #Svar = np.zeros(Nt+1) # not needed

    #Add code here
    N = nx.number_of_nodes(G) 
    # initial values
    s0 = np.zeros(N) 
    i0 = np.zeros(N)
    v0 = np.ones(N)
    s0[x] = 0.1
    i0[x] = 0.05
    v0[x] = 0.05
    
    y0 = np.concatenate([s0,i0,v0]) # form inital value vector
    
    A = nx.adjacency_matrix(G) # adjacency matrix - scipy.sparse.csr
    # contruction of the Flux matrix, F
    q = np.array([y for x,y in G.degree()]) # vector of degree
    qDiag = sp.sparse.diags(q) # diagonal matrix of degree
    num = qDiag*A # 'numerator' of F ### '*' operator is overloaded
    denom = sp.sparse.diags(1/(A*q)) #'denominator' of F as sparse diag
    F = tau*num*denom # F 
    
    
    def RHS(y,t):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Discussion:
        
        The number of operations for computing dSi/dt i = 0,..,N-1 in RHS is
        9N + 8M + 1. The complexity is then O(N+M), where the graph has N nodes
        and M edges. y has length 3N.
        
        This calculation is as follows.
        
        1. For dS the vectors S and I are required. Retrieving two N length slices
            of y requires 2N operations.
        2. Computing theta is not required for dS.
        3. Computing sv is not required for dS.
        
        To compute ds there are five types of operations
        1) scalar*vector, 2) matrix*vector, 3) matrix*matrix, 4) sum of matrix rows,
        5) scalar + scalar.
    
        1) The number of operations for scalar*vector is N.
        2) Since the matrices are in sparse format, the number of operations
            for matrix-vector multiplication is related to the number of non-zero entries.
            For each non-zero entry we have a multiplication and addition.
            In this case the matrix is F which has 2M non-zero entries, the same as
            the adjacency matrix. So we have 4M operations.
        3) For the matrix-matrix multiplication we have a sparse matrix * a sparse diagonal
            matrix so the number of operations is 2M corresponding to multiplication
            of non-zero entries in each column by a scalar.
        4) The sum of matrix rows is also 2M due to sparse format.
        5) The number of operations for scalar + scalar is 1.
    
        We also have a conversion of a vector to a sparse diagonal matrix. We will
        assume this takes N operations for assignment of values to the diagonal.
        
        4. Computing the components of ds takes N + (1+N) + 4M + (2M+2M +N)
            = 3N + 8M +1 operations.
        5. Summing the components requires another 4N operations.
        
        The speed of the computation is boosted significantly by having F
        precomputed. Note that the operations considered here are operations on
        the vectors and matrices. The actual implementation almost certainly 
        requires more operations behind the scenes, such as incrementing indeces
        and so forth.
    
        """
        # separate into types
        S = y[:N]
        I = y[N:2*N]
        V = y[2*N:]
        
        theta = theta0 + theta1*(1-np.sin(2*np.pi*t)) # compute theta
        sv = S*V # compute S*V
        
        # compute RHS for each type
        ds = a*I - (g+k)*S + F*S - sp.sum(F*sp.sparse.diags(S),axis = 0)
        di = theta*sv - (k+a)*I + F*I - sp.sum(F*sp.sparse.diags(I),axis = 0)
        dv = k*(1- V) - theta*sv + F*V - sp.sum(F*sp.sparse.diags(V),axis = 0)
        
        dy = np.ravel(np.concatenate([ds,di,dv])) # return a single vector
        return dy

    y = odeint(RHS,y0,tarray) # use odeint to time march
    Smean = np.mean(y[:,:N],axis = 1) # means over time
    Svar = np.var(y[:,:N],axis = 1) # variance over time
    if display: # plots of mean and variance
        plt.figure()
        plt.plot(tarray,Smean)
        plt.xlabel('t')
        plt.ylabel('<S(t)>')
        plt.title("Mean of S over time")
        plt.figure()
        plt.plot(tarray,Svar)
        plt.xlabel('t')
        plt.ylabel('Var(S(t))')
        plt.title("Variance of S over time")
    return Smean,Svar


def diffusion():
    """Analyze similarities and differences
    between simplified infection model and linear diffusion on
    Barabasi-Albert networks.
    Modify input and output as needed.

    Discussion:
    
    The plots generated for this discussion use a B-A random graph (m=5, n= 100),
    where the models have been run up to time (tf) = 500 unless stated otherwise.
    The diffusion parameter D is taken to be D = 0.01 = tau. theta0 = 80 unless
    indicated otherwise. All other parameters (a,g,k, theta1) are 0. The starting
    node is always 0.
    
    Asymptotic distribution and initial conditions
    
    Both the linear and non-linear models will converge to an asymptotic distribution
    across the nodes which corresponds to an equilibrium in in the flux across each
    node.
    
    The linear model has the functional form dy/dt = -DLy where L is the Lagrangian
    matrix, D is a parameter and y is the vector of S, I and V for each state.
    Assuming one connected component, for the linear diffusion model, L is symmetric,
    so has non-negative real eigenvalues, and has a zero eigenvalue corresponding
    to the eigenvector of ones. Since the solution to the ODE is Cexp(-DLt) it is
    clear that the “fraction” of cells of each type in each node converges to the
    mean value for each type, S, I and V. The mean value can be found from the
    initial conditions. Figure 1 shows the convergence to a steady state for S
    cells, for a sample of nodes. Figure 2 shows the end values for S, I and V
    cells against node degree. As predicted the values are uniform across all cells.
    
    The non-linear model, however, assumes movement of cells from type V to I,
    which is controlled by the parameter theta (= theta0). Assuming theta > 0, cells
    will be removed from V and added to I, so V -> 0 as t -> infinity, and I will
    converge to a steady value independent of theta. Figure 3 shows how I varies
    over time for different values of theta for a sample of nodes. We can see that
    I converges at different rates for different thetas, but always to the same
    value. Note that for very small theta, convergence is very slow. Figure 3 also
    shows that the limiting distribution however is not all nodes to the mean;
    each node converges to a different value. Figures 4 to 6 show the final values
    for S, I and V for each node, against the degree of the node. We note that all
    three types show a non-linear relationship between the fraction of cells and
    degree, with S and I showing a positive and V showing a negative relationship.
    This indicates that nodes with higher degree tend to “hang on” to cells they
    receive, which may be a desirable feature of the model. Nodes with higher degree
    have smaller V values since they have larger S values which increases the rate
    of conversion to I. However, all V values should go to 0 eventually.
    
    Speed of convergence and variance
    
    The linear model converges exponentially quickly to 0, as expected from the
    form of the ODE solution. The non-linear model also converges, to a non zero
    value, but far slower than the linear model. Figure 7 shows the convergence
    of  Var(S) for both models which gives an indication of the speed of convergence.

   

    """
    G = nx.barabasi_albert_graph(n=100,m=5) # generate graph
    N = nx.number_of_nodes(G) 
    ylin = diff(G,0.01,tf=500) # run linear diffusion
    ynlin = mdlN(G,tf=500) # run simplified non-linear model
    tarray = np.linspace(0,500,501) # time array
    q = np.array([y for x,y in G.degree()]) # vector of degree
    
    ### Figure 1 - Linear convergence ###
    
    plt.figure()
    legend = []
    for i in range(10):
        plt.plot(tarray,ylin[:,i*10]) # sample of 10 nodes from S
        legend.append(i*10)
    plt.xlabel("t")
    plt.ylabel("Fraction of initial cells - S")
    plt.title("Daniel Trent - 01051314 - Diffusion \n Figure 1 - Linear model convergence to mean - S")
    plt.legend(legend,title = "Node")
    
    
    ### Figure 2 - Linear convergence - vs degree ###
    
    plt.figure()
    plt.scatter(q,ylin[500,N:2*N],alpha=0.75,marker = "v") # values from final timestep I
    plt.scatter(q,ylin[500,2*N:],alpha=0.75,marker = "x") # V
    plt.scatter(q,ylin[500,:N],alpha=0.75,marker = "*",color = "k") #S
    plt.xlabel("Node degree")
    plt.ylabel("Fraction of initial cells")
    plt.title("Daniel Trent - 01051314 - Diffusion \n Figure 2 - Linear model limiting distribution")
    plt.legend(["I","V","S"])
    
    
    ### Figures 4-5 - Non linear convergence ###
    plt.figure()
    plt.scatter(q,ynlin[500,:N])
    plt.xlabel("Node degree")
    plt.ylabel("Fraction of initial cells")
    plt.title("Daniel Trent - 01051314 - Diffusion \n Figure 4 - Non-Linear model limiting distribution S")
    
    plt.figure()
    plt.scatter(q,ynlin[500,N:2*N])
    plt.xlabel("Node degree")
    plt.ylabel("Fraction of initial cells")
    plt.title("Daniel Trent - 01051314 - Diffusion \n Figure 5 - Non-Linear model limiting distribution I")
    
    plt.figure()
    plt.scatter(q,ynlin[500,2*N:])
    plt.xlabel("Node degree")
    plt.ylabel("Fraction of initial cells")
    plt.title("Daniel Trent - 01051314 - Diffusion \n Figure 6 - Non-Linear model limiting distribution V")
    
    
    ### Figure 7 -Variance ###
    lvar = np.var(ylin[:,:N],axis = 1) # compute variances across nodes for each timestep
    nlvar = np.var(ynlin[:,:N],axis = 1)
    plt.figure()
    plt.plot(tarray,lvar)
    plt.plot(tarray,nlvar)
    plt.xlabel("t")
    plt.ylabel("Var(S)")
    plt.title("Daniel Trent - 01051314 - Diffusion \n Figure 7 - Variance of S model comparison")
    plt.legend(["Linear","Non-linear"],title = "Model")
    
    ### Figure 3 - Dependency on theta ### 
    ainit = np.array([0.1,0.5,1,10,100])
    isnlin = np.zeros((501,5,5))
    for i in range(5):
        ynlin = mdlN(G,Y0 = (0.1,0.05,0.05),params=(0,ainit[i],0,0,0,0.01),tf=10000)
        isnlin[:,:,i] = ynlin[:,N:N+5]
    tarray1 = np.linspace(0,10000,501)
    plt.figure()
    for i in range(4):
        plt.subplot(220+i+1)
        for j in range(5):
            plt.plot(tarray1,isnlin[:,i,j])
    plt.suptitle("Daniel Trent - 01051314 - Diffusion \n Figure 3 - I dependency on theta (sample of nodes)")
    plt.figlegend([0.1,0.5,1,10,100],title="theta")
    fig = plt.gcf()
    fig.text(0.5, 0.04, 't', ha='center')
    fig.text(0.04, 0.5, 'Fraction of initial cells', va='center', rotation='vertical')
    
    return None 

def diff(G,D,x = 0,Y0 = (0.1,0.05,0.05),tf=6,Nt=500,display=False):
        """ Diffusion model
        HELPER FUNCTION FOR DIFFUSION
        """
        N = nx.number_of_nodes(G)
        s0 = np.zeros(N)
        i0 = np.zeros(N)
        v0 = np.ones(N)
        s0[x] = Y0[0]
        i0[x] = Y0[1]
        v0[x] = Y0[2]
        y0 = np.concatenate([s0,i0,v0])
        
        L = nx.laplacian_matrix(G)
        tarray = np.linspace(0,tf,Nt+1)
        def RHS(y,t):
            S = y[:N]
            I = y[N:2*N]
            V = y[2*N:]
            
            ds = -D*L*S
            di = -D*L*I
            dv = -D*L*V
            
            dy = np.ravel(np.concatenate([ds,di,dv]))
            return dy
        y = odeint(RHS,y0,tarray)
        return y
    
def mdlN(G,x=0,Y0 = (0.1,0.05,0.05),params=(0,80,0,0,0,0.01),tf=6,Nt=500):
    """ Non-linear model with all params bar theta0,tau =0
    HELPER FUNCTION FOR DIFFUSION
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    
    N = nx.number_of_nodes(G)
    s0 = np.zeros(N)
    i0 = np.zeros(N)
    v0 = np.ones(N)
    s0[x] = Y0[0]
    i0[x] = Y0[1]
    v0[x] = Y0[2]
    y0 = np.concatenate([s0,i0,v0])
    
    A = nx.adjacency_matrix(G) # adjacency matrix - scipy.sparse.csr
    # contruction of the Flux matrix
    q = np.array([y for x,y in G.degree()]) # vector of degree
    qDiag = sp.sparse.diags(q) # diagonal matrix of degree
    num = qDiag*A # 'numerator' of F ### '*' operator is overloaded
    denom = sp.sparse.diags(1/(A*q)) #'denominator' of F as sparse diag
    F = tau*num*denom # Flux matrix, order of num and denom is important 
    
    
    def RHS(y,t):
        S = y[:N]
        I = y[N:2*N]
        V = y[2*N:]
        
        theta = theta0 + theta1*(1-np.sin(2*np.pi*t))
        tsv = theta*S*V
        
        ds = a*I - (g+k)*S + F*S - sp.sum(F*sp.sparse.diags(S),axis = 0)
        di = tsv - (k+a)*I + F*I - sp.sum(F*sp.sparse.diags(I),axis = 0)
        dv = k*(1- V) - tsv + F*V - sp.sum(F*sp.sparse.diags(V),axis = 0)
        
        dy = np.ravel(np.concatenate([ds,di,dv]))#modify
        return dy

    y = odeint(RHS,y0,tarray)

    return y

if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    a = diffusion() #modify as needed
