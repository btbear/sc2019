# -*- coding: utf-8 -*-
"""
Created on Thu Feb 28 22:37:57 2019

@author: dantr
"""

G = nx.barabasi_albert_graph(n=100,m=5)
N = nx.number_of_nodes(G)
ylin = diff(G,0.01,tf=500)
ynlin = mdlN(G,tf=500)
tarray = np.linspace(0,500,401)
q = np.array([y for x,y in G.degree()])

### Figure 1 - Linear convergence ###

plt.figure()
legend = []
for i in range(10):
    plt.plot(tarray,ylin[:,i*10])
    legend.append(i)
plt.xlabel("t")
plt.ylabel("Fraction of initial cells")
plt.title("Figure 1 - Linear model convergence to mean")
plt.legend(legend,title = "Node")


### Figure 2 - Linear convergence 2 ###

plt.figure()
plt.scatter(q,ylin[400,N:2*N])
plt.scatter(q,ylin[400,2*N:])
plt.scatter(q,ylin[400,:N])
plt.xlabel("Node degree")
plt.ylabel("Fraction of initial cells")
plt.title("Figure 2 - Linear model limiting distribution")
plt.legend(["S","I","V"])


### Figures 4-5 - Non linear convergence ###
plt.figure()
plt.scatter(q,ynlin[400,:N])
plt.xlabel("Node degree")
plt.ylabel("Fraction of initial cells")
plt.title("Figure 4 - Non-Linear model limiting distribution S")
#plt.legend(["S","I","V"])

plt.figure()
plt.scatter(q,ynlin[400,N:2*N])
plt.xlabel("Node degree")
plt.ylabel("Fraction of initial cells")
plt.title("Figure 5 - Non-Linear model limiting distribution I")
plt.figure()
plt.scatter(q,ynlin[400,2*N:])
plt.xlabel("Node degree")
plt.ylabel("Fraction of initial cells")
plt.title("Figure 6 - Non-Linear model limiting distribution V")


### Figure 7 -Variance ###
lvar = np.var(ylin[:,:N],axis = 1)
nlvar = np.var(ynlin[:,:N],axis = 1)
plt.figure()
plt.plot(tarray,lvar)
plt.plot(tarray,nlvar)
plt.xlabel("t")
plt.ylabel("Var(S)")
plt.title("Figure 7 - Variance of S model comparison")
plt.legend(["Linear","Non-linear"],title = "Model")

### Figure 3 - Dependency on theta ### 
ainit = np.array([0.1,0.5,1,10,100])
isnlin = np.zeros((401,5,5))
for i in range(5):
    ynlin = mdlN(G,Y0 = (0.1,0.05,0.05),params=(0,ainit[i],0,0,0,0.01),tf=10000)
    isnlin[:,:,i] = ynlin[:,N:N+5]
tarray1 = np.linspace(0,10000,401)
plt.figure()
for i in range(4):
    plt.subplot(220+i+1)
    for j in range(5):
        plt.plot(tarray1,isnlin[:,i,j])
plt.suptitle("Figure 3 - I dependency on theta \n sample of nodes shown")
plt.figlegend([0.1,0.5,1,10,100],title="theta")
fig = plt.gcf()
fig.text(0.5, 0.04, 't', ha='center')
fig.text(0.04, 0.5, 'Fraction of initial cells', va='center', rotation='vertical')