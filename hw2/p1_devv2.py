"""M345SC Homework 2, part 1
Daniel Trent 01051314
"""

def scheduler(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list my also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    S: A list of integers corresponding to the schedule of tasks. S[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Discussion:
    
    Consider the tasks as nodes in a graph. Then dependencies are directed edges
    such that i <- j if i is a depedency for j.
    We can then use a modified version of depth first search (DFS) to find the order of 
    dependencies. In particular, the day of task i is 1 more than the day of its 
    largest dependent. 
    We can compute the day for each node recursively by setting the day of each task 
    to be the maximum plus 1 of the respective days of all its dependent tasks. The base
    case is a task with no dependencies .This allows us to compute days for each task as
    we go along, so each task is only explored once.
    The DFS algorithm relies on having only one connected component, so the algorithm
    will need to be applied to all tasks, to account for multiple possible components.
    
    The recursive algorithm runs as follows:
        Algorithm Compute day:
        1. IF Node is explored: DO nothing
        2. ELSE: Set Node as explored
        3.    IF Node has no dependencies:
        4.        Set node day to 0
        5.    ELSE: max <- 0
        6.        FOR each dependent node v:
        7.            IF v is unexplored:
        8.                Compute day for v
        9.            IF day > max:
        10.              max <- day
        11. RETURN max + 1
        
    For this function each node is explored only once. Each edge is encountered once
    (as they are directed). The asysmptotic complextity is therefore O(N+D) where D
    is the number of edges/dependencies. D can be considered a multiple of M where M
    is the number of nodes that have dependencies so the complexity is O(N+M) which
    is expected from a DFS-like method.    
        
    
    """

    
    def sch_helper(L,node,Lexplore,S):
        """" 
        Helper function for scheduler. Provides a recursive method for 
        computing the schedule day for 'node' and all its dependencies.
        
        Input:
            L: Dependency list
            node: starter node
            Lexplore: List of flags for nodes explored
            S: List of schedule days
        Output:
            S, Lexplore - modified
        """
        if Lexplore[node]==1: # if explored
            return S,Lexplore # do nothing
        Lexplore[node]=1 # else set node as explored
        if L[node] == []: # Base case - no dependencies
            S[node] = 0 # schedule day 0
        else: # if depencies
            maxm = 0 # initialise max
            for v in L[node]: # for each dependent task
                if Lexplore[v] == 0: # if unexplored
                    S,Lexplore = sch_helper(L,v,Lexplore,S) # recursive call to explore
                # set max to be the larger of current or new node day
                maxm = max([maxm,S[v]]) 
                
            S[node] = maxm+1 # set day of node to be one more than largest dependency
        return S,Lexplore # return lists that have been modified
    
    ###### Main Function##########
    N = len(L)
    Lexplore = [0]*N #initialise list of explored flags
    S=[0]*N # initialise list of schedule days
    for i in range(N): # iterate over all nodes
        S,Lexplore = sch_helper(L,i,Lexplore,S) # update lists
    return S


def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal
    from node J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2 from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Discussion: 
    
    Since the signal is boosted back to a0 at each junction, a path is valid if it
    contains no edges i<->j such that Lij < amin/a0.
    Thus this problem can be reduced to finding a path from J1 to J2 through an unweighted
    network where edges with Lij < amin/a0 have been removed. This can be accomplished
    with a standard breadth first search (BFS) adjusted to ignore edges with low weight.
    Since this is a standard BFS algorithm with one extra comparison for each edge, the
    algorithm has O(N+M) complexity. Since we have a target node, the algorithm can be 
    terminated early when a path is found, which could result in significant savings for 
    very large graphs.
    
    
    """
    from collections import deque
    
    minval = amin/a0 # set minimum weight
    
    
    Lexp = [0 for l in A] #flags

    L = [[] for l in A] #paths
    
    Q = deque() # initialise queue
    Q.append(J1) # start queue with J1
    Lexp[J1]=1 #set J1 as explored
    
    L[J1]=[J1] # start path
    while len(Q)>0:
        x = Q.popleft() #remove node from front of queue
        
        for v,l in A[x]:
            if (Lexp[v]==0)&(l>=minval): # if unexplored and weight is sufficient
                Q.append(v) #add unexplored neighbors to back of queue
                Lexp[v]=1 # set v as explored
                L[v].extend(L4[x]) #Add path to node x and node v to path
                L[v].append(v)     #for node v
                if v == J2: # terminate early if path found
                    return L[J2]
    
    return L[J2]


def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency list, A)

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Discussion: 
    
    We will refer to the junctions as nodes and connections as edges in a graph.
    Since the signal is boosted back to a0 at each junction, the required initial
    amplitude, a0, for any particular path is given by amin/L_p where L_p is the 
    minimum loss value of any connection in the path.
    a0min is therefore given by amin/L_max where L_max is the largest L_p for ALL
    paths connecting J1 and J2.
    
    Algorithm:
        
    The algorithm is a modified version of Dijkstra's algorithm, where the definition
    of "distance" is modified to mean the value of the minimum loss. Thus, rather than
    finding the "shortest path", we find the path with the largest minimum loss.
    This definition prompts a number of changes to the algorithm.
    1) When moving a node from unexplored to explored, we choose the node with the 
        largest provisional distance, as opposed to the shortest. This means that all
        nodes are started with negative, as opposed to large, provisional distances.
    2) The provisional distance for a node is the minimum of the provisional distance
        for the adjacent node and the loss of the connection with the adjacent node.
    3) There are a couple of special cases to consider when updating provisional distances
        relating to whether a node has previously been found, and if the adjacent node is
        the source node, J1.
        
    Since we have a target node the algorithm can be terminated early if J2 becomes marked
    explored, or is found to be unreachable.
    Another addition to the algorithm is reporting back the path. This requires keeping track
    of the previous to each node in the path, which allows a path to be traced back. This does
    not add additional complexity to the algorithm asymptotically.    
        
    Implementation:
    The algorithm is implemented using dictionaries to keep track of provisional distances.
    Since searching for the maximum in a dictionary is O(N), the algorithm as a whole is O(N^2).
    Using a priority queue, such as a binary heap, would allow for O(log(N)) removal of a minimum
    which would reduce the complexity to O(Nlog(N)). This method would require the algorithm
    to more closely resemble the traditional Dijkstra's algorithm, where we search for minimum
    distance. This can be accomplished, for example by defining "distance" to be the inverse of
    the minimum loss, 1/L_p. The efficiency of the priority queue is also affected by the speed of
    updating the provisional distances. This will need to be at least O(log(N)) to realise gains
    in time complexity. (This is not implemented natively for the heapq structure in Python).
    (The algorithm for retrieving the path is O(N) so does not affect the asymptotic complexity).
    
    """
    # degenerate case J1==J2
    if J1 == J2:
        #print("Gotta go somewhere")
        return 0,[]
    
    def getPath(Parent,J1,J2):
    """ Helper function for getting the path if one can be found
    """
        p = [] # path
        u = J2  # start at the end
        while u != J1: # iterate back
            p.append(u) # add to path
            u = Parent[u] # update node to be parent of prev
        return [J1]+p[::-1] # return path with source in correct order
    
    N = len(A)
    #Initialize dictionaries
    dinit = -1 # negative distances 
    Edict = {} #Explored nodes
    Udict = {} #Unexplored nodes
    Parent = {} # Parents - nodes that come before other nodes in path from J1
    
    for n in range(N): # for each node
        Udict[n] = dinit # set initial weight
    Udict[J1]=0 # distance of source is 0

    #Main search
    while len(Udict)>0: # while nodes are unexplored
        #Find node with max weight in Udict and move to Edict
        dmax = -1 
        for n,w in Udict.items(): # all unexplored nodes
            if w>dmax: # find largest distance
                dmax=w # update distance
                nmax=n # select node
        
        # deal with unreachable nodes
        if dmax == -1: # if no nodes with positive distance
            return -1,[] # J2 is unreachable
        
        if nmax == J2: # if J2 has largest weight best path has been found
            aout = amin/dmax # set a0min
            path = getPath(Parent,J1,J2) # get path
            return aout,path
        
        Edict[nmin] = Udict.pop(nmin) # move node to explored
        
        
        #Update provisional distances for unexplored neighbors of nmin
        for n,w in A[nmax]: # for each adjacent node
            if n in Udict: # if unexplored
                dcomp = min([dmax, w]) # set min of current dist. and 'a' of new edge
                
                if Udict[n]==-1: # if node has not been reached previously
                    if dmax == 0: # special case if directly found from J1
                        Udict[n]=w # set distance
                        Parent[n] = nmax # set parent
                    else: # if not directly found from J1
                        Udict[n]=dcomp #set distance
                        Parent[n] = nmax # set parent
                elif dcomp>Udict[n]: # if node has been previously found and new dist is larger
                    Udict[n]=dcomp # update dist
                    Parent[n] = nmax # update parent


if __name__=='__main__':
    #add code here if/as desired
    w=None #modify as needed
