"""Dijkstra algorithm implemented using dictionaries.
Note: Implementation is different from lecture 8 slides but similar
to the in-class example.
"""
import networkx as nx

def dijkstra(A,amin,J1,J2):
    """Find shortest distances to s in weighted graph, G"""
    if J1 == J2:
        print("Gotta go somewhere")
        return -1,[],0
    def getPath(Parent,J1,J2):
        p = []
        u = J2
        while u != J1:
            p.append(u)
            u = Parent[u]
        return [J1]+p[::-1]
    
    N = len(A)
    #Initialize dictionaries
    dinit = -1#10**6
    Edict = {} #Explored nodes
    Udict = {} #Uneplroed nodes
    Parent = {}
    for n in range(N):
        Udict[n] = dinit
    Udict[J1]=0

    #Main search
    while len(Udict)>0:
        #Find node with min d in Udict and move to Edict
        dmin = -1
        for n,w in Udict.items():
            if w>dmin:
                dmin=w
                nmin=n
        #print(dmin)
        if dmin == -1:
            return -1,[],0
        
        if nmin == J2:
            aout = amin/dmin
            path = getPath(Parent,J1,J2)
            return aout,path,dmin
        Edict[nmin] = Udict.pop(nmin)
        #print("moved node", nmin)
        
        #Update provisional distances for unexplored neighbors of nmin
        for n,w in A[nmin]:
            if n in Udict:
                dcomp = min([dmin, w])
                if Udict[n]==-1:
                    if dmin == 0:
                        Udict[n]=w
                        Parent[n] = nmin
                    else:
                        Udict[n]=dcomp
                        Parent[n] = nmin
                elif dcomp>Udict[n]:
                    Udict[n]=dcomp
                    Parent[n] = nmin
        #print(Udict)
        


if __name__=='__main__':
    #Example from lecture 8 slides
    A = [[],[(2,0.5),(5,0.6)],[(1,0.5),(5,0.9),(3,0.7)],[(2,0.7),(4,0.2)],[(3,0.2),(5,0.3),(6,0.9)],[(1,0.6),(2,0.9),(4,0.3)],[(4,0.9)]]
    a0min, path,dmin = dijkstra(A,0.5,1,4)
    