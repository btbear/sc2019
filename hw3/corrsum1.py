# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 11:56:07 2019

@author: dantr
"""

epsilon = np.logspace(-3,1.1)
dt = T/Nt
f1m = f1[mean_c1==mean_c1.max()]*Nt/T
f2m = f2[mean_c2==mean_c2.max()]*Nt/T

tau1 = 1/(5*abs(f1m))
tau2 = 1/(5*abs(f2m))


Del1 = int(tau1/dt)
Del2 = int(tau2/dt)
#print(Del,x.size)
v1 = np.vstack([g1[:-2*Del1,100],g1[Del1:-Del1,100],g1[2*Del1:,100]]).T
v2 = np.vstack([g2[:-2*Del2,100],g2[Del2:-Del2,100],g2[2*Del2:,100]]).T
   

D1 = sp.spatial.distance.pdist(v1)
D2 = sp.spatial.distance.pdist(v2)
C1 = np.zeros(50)
C2 = np.zeros(50)
for i in range(len(epsilon)):
    D3 = D1[D1<epsilon[i]]
    D4 = D2[D2<epsilon[i]]
    C1[i] = D3.size
    C2[i] = D4.size


eps1 = epsilon[C1>0]
C1 = C1[C1>0]
eps2 = epsilon[C2>0]
C2 = C2[C2>0]

z1 = np.polyfit(np.log(eps1),np.log(C1),1)
z2 = np.polyfit(np.log(eps2),np.log(C2),1)
print(z1)
print(z2)

plt.figure()
plt.plot(np.log(eps1),np.log(C1),'*')
plt.title("Correlation sum A")
plt.xlabel("$\log(\epsilon)$")
plt.ylabel("$\log(C(\epsilon))$")
plt.figure()
plt.plot(np.log(eps2),np.log(C2),'*')
plt.title("Correlation sum B")
plt.xlabel("$\log(\epsilon)$")
plt.ylabel("$\log(C(\epsilon))$")


