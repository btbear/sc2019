"""M345SC Homework 3, part 2
Daniel Trent 01051314
"""
import numpy as np
import networkx as nx
from scipy.linalg import expm

def growth1(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.1
    Find maximum possible growth, G=e(t=T)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion: Add discussion here

    We note that model can be expressed in the form of a linear ODE which takes
    the form dy/dt = Ay where A is a matrix. The solution is then y = exp(At)*y0
    where we are using the matrix exponential and y0 is a vector of initial
    conditions.
    
    We also note that e(t) = y^T*y where y is the solution vector at time t.
    Thus the problem is to find the vector y0 such that |(exp(At)*y0)|^2/|y0|^2
    is the largest possible value. Let B = exp(At). From lectures we have that
    the solution is then the eigenvector corresponding to the largest eigenvalue
    of B^T*B. These can be found using SVD.
    
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)

    #Add code here
    # Construct ODE matrix A
    
    # Terms involving F
    A = np.kron(np.eye(3),F) # block diagonal of F 3 times
    
    # second term in F - involves only single node
    sumF = np.sum(F,axis = 0)
    for i in range(N):
        A[i,i] -= sumF[i]
        A[N+i,N+i] -= sumF[i]
        A[2*N+i,2*N+i] -= sumF[i]
        
    # Terms not involving F
    for i in range(N):
        A[i,i] += -(g+k)
        A[i,N+i] += a
        A[N+i,N+i] += -(k+a)
        A[N+i,i] += theta
        A[2*N+i,2*N+i] += k
        A[2*N+i,i] += -theta
        
    
    # solution to ODE using matrix exponential
    #d, Y = np.linalg.eig(A)
    #Yinv = np.linalg.pinv(Y)
    #D = np.diag(np.exp(d*T))
    #B = Y@D@Yinv
    B = expm(A*T)
    
    # Find eigenvalue and egenvector using SVD
    u,s,vh = np.linalg.svd(B)
    
    G = s[0]**2
    y = vh[0]
    
    return G,y

def growth2(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.2
    Find maximum possible growth, G=sum(Ii^2)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion: Add discussion here
    
    For this part we want the maximal growth of I only. This can be achieved by
    selecting the middle part of the matrix B = exp(A*t) which corresponds to
    the vector I. We can again use SVD to obtain the required values.
    
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0
    y = np.zeros(3*N)

    #Add code here
    
    # Construct ODE matrix A
    # Terms involving F
    A = np.kron(np.eye(3),F) # block diagonal of F 3 times
    
    # second term in F - involves only single node
    sumF = np.sum(F,axis = 0)
    for i in range(N):
        A[i,i] -= sumF[i]
        A[N+i,N+i] -= sumF[i]
        A[2*N+i,2*N+i] -= sumF[i]
        
    # Terms not involving F
    for i in range(N):
        A[i,i] += -(g+k)
        A[i,N+i] += a
        A[N+i,N+i] += -(k+a)
        A[N+i,i] += theta
        A[2*N+i,2*N+i] += k
        A[2*N+i,i] += -theta
        
    
    # solution to ODE using matrix exponential
    #d, Y = np.linalg.eig(A)
    #Yinv = np.linalg.pinv(Y)
    #D = np.diag(np.exp(d*T))
    #B = Y@D@Yinv
    B = expm(A*T)
    
    C = B[N:2*N,:]
    # Find eigenvalue and egenvector using SVD
    u,s,vh = np.linalg.svd(C)
    
    y = vh[0]
    G = s[0]**2
#    p = C@y
#    m = s[0]**2
#    G = (p.T@p)/(y.T@y)
    

    return G,y


def growth3(G,params=(2,2.8,1,1.0,0.5),T=6):
    """
    Question 2.3
    Find maximum possible growth, G=sum(Si Vi)/e(t=0)
    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth

    Discussion: Add discussion here
    
    For the numerator of G we have sum(Si*Vi) which is the inner product of the
    vectors S and V. By the Cauchy-Schwartz inequality this is smaller than or
    equal to the norm of S multiplied by the norm of V which gives the upper bound
    we are looking for. The maximum growth in S and V can be computed in the same
    manner as we computed growth in I in 'growth2'.
    
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    G=0

    #Add code here
    
    # Construct ODE matrix A
    # Terms involving F
    A = np.kron(np.eye(3),F) # block diagonal of F 3 times
    
    # second term in F - involves only single node
    sumF = np.sum(F,axis = 0)
    for i in range(N):
        A[i,i] -= sumF[i]
        A[N+i,N+i] -= sumF[i]
        A[2*N+i,2*N+i] -= sumF[i]
        
    # Terms not involving F
    for i in range(N):
        A[i,i] += -(g+k)
        A[i,N+i] += a
        A[N+i,N+i] += -(k+a)
        A[N+i,i] += theta
        A[2*N+i,2*N+i] += k
        A[2*N+i,i] += -theta
        
    
    # solution to ODE using matrix exponential
    #d, Y = np.linalg.eig(A)
    #Yinv = np.linalg.pinv(Y)
    #D = np.diag(np.exp(d*T))
    #B = Y@D@Yinv
    B = expm(A*T)
    
    C1 = B[:N,:] # S
    C2 = B[2*N:,:] #V
    # Find eigenvalue and egenvector using SVD
    u1,s1,vh1 = np.linalg.svd(C1)
    u2,s2,vh2 = np.linalg.svd(C2)
    
    #y = vh[0]
    #p = C@y
    #G = (p.T@p)/(y.T@y)
    G = s1[0]*s2[0]
    return G


def Inew(D):
    """
    Question 2.4

    Input:
    D: N x M array, each column contains I for an N-node network

    Output:
    I: N-element array, approximation to D containing "large-variance"
    behavior

    Discussion: Add discussion here
    """
    # N,M = D.shape
    # I = np.zeros(N)

    #Add code here
    #Scale to zero mean

    A = D - D.mean(axis=0, keepdims=True)
    #A = A.T
    # create covariance matrix
    print(A.shape)
    
    # find projection matrix onto 1 dim
    
    u,s,vh = np.linalg.svd(A)
    print(vh.shape)
    #G =u.T@A
    #print(G.shape)
    G = A@vh.T
    print(G.shape)
    I = G[:,0]
    

    return I


if __name__=='__main__':
    #lG=None
    #add/modify code here if/as desired
    N,M = 100,5
    G = nx.barabasi_albert_graph(N,M,seed=1)
    gr,r = growth2(G)
    print(gr)