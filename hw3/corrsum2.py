# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 11:56:07 2019

@author: dantr
"""

epsilon = np.logspace(-3,1.1)
dt = T/Nt
f1m = f1[mean_c1==mean_c1.max()]*Nt/T
f2m = f2[mean_c2==mean_c2.max()]*Nt/T

tau1 = 1/(5*abs(f1m))
tau2 = 1/(5*abs(f2m))


Del1 = int(tau1/dt)
Del2 = int(tau2/dt)
#print(Del,x.size)
C1 = np.zeros(50)
C2 = np.zeros(50)

for j in range(Nx):
    v1 = np.vstack([g1[:-2*Del1,j],g1[Del1:-Del1,j],g1[2*Del1:,j]]).T
    v2 = np.vstack([g2[:-2*Del2,j],g2[Del2:-Del2,j],g2[2*Del2:,j]]).T
       
    
    D1 = sp.spatial.distance.pdist(v1)
    D2 = sp.spatial.distance.pdist(v2)

    for i in range(len(epsilon)):
        D3 = D1[D1<epsilon[i]]
        D4 = D2[D2<epsilon[i]]
        C1[i] += D3.size
        C2[i] += D4.size

C1 = C1/Nx
C2 = C2/Nx

eps1 = epsilon[C1>0]
C1 = C1[C1>0]
eps2 = epsilon[C2>0]
C2 = C2[C2>0]

z1 = np.polyfit(np.log(eps1)[3:-12],np.log(C1)[3:-12],1)
z2 = np.polyfit(np.log(eps2)[3:-12],np.log(C2)[3:-12],1)
print(z1)
print(z2)

plt.figure()
plt.plot(np.log(eps1),np.log(C1),'*')
plt.plot(np.log(eps1)[3:-12], z1[0]*np.log(eps1)[3:-12] + z1[1])
plt.title("Correlation sum A")
plt.xlabel("$\log(\epsilon)$")

plt.legend(['$C(\epsilon)$',"Polynomial fit slope="+str(round(z1[0],3))])

plt.figure()
plt.plot(np.log(eps2),np.log(C2),'*')
plt.plot(np.log(eps2)[3:-12], z1[0]*np.log(eps2)[3:-12] + z1[1])
plt.title("Correlation sum B")
plt.xlabel("$\log(\epsilon)$")

plt.legend(['$C(\epsilon)$',"Polynomial fit slope="+str(round(z2[0],3))])

