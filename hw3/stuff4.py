# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 12:25:46 2019

@author: dantr
"""


epsilon = np.logspace(0,1.3)
    
n1 = g1.shape[0]

d1 = 2/(n1*(n1-1))
    
D1 = sp.spatial.distance.pdist(g1)
D2 = sp.spatial.distance.pdist(g2)
C1 = np.zeros(50)
C2 = np.zeros(50)
for i in range(len(epsilon)):
    D3 = D1[D1<epsilon[i]]
    D4 = D2[D2<epsilon[i]]
    C1[i] = D3.size *d1
    C2[i] = D4.size *d1


eps1 = epsilon[C1>0]
C1 = C1[C1>0]
eps2 = epsilon[C2>0]
C2 = C2[C2>0]

z1 = np.polyfit(np.log(eps1),np.log(C1),1)
z2 = np.polyfit(np.log(eps2),np.log(C2),1)
print(z1)
print(z2)

plt.figure()
plt.loglog(eps1,C1,'*')
plt.figure()
plt.loglog(eps2,C2,'*')