"""M345SC Homework 3, part 1
Daniel Trent 01051314
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.signal import hann
import scipy as sp

def nwave(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        """
        g = f[:Nx]+1j*f[Nx:]
        #add code here
        #d2g=?
        c = np.fft.fft(g)
        n = np.fft.fftshift(np.linspace(-Nx/2,Nx/2-1,Nx))
        k = 2*np.pi*n/L
        d2g = np.fft.ifft(-(k**2)*c)
        
        
        #-----------
        dgdt = alpha*d2g + g -beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    f0=np.zeros(2*Nx)
    f0[:Nx]=g0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')


    return g

def analyze():
    """
    Question 1.2
    Add input/output as needed

    Discussion: Add discussion here
    
    Fourier transform shows clear signs of chaos
    
    """
    L = 100
    Nx = 256
    Nt = 801
    T = 200

    cutoff = int(50/T *(Nt-1))
    
    g1 = nwave(1-2j,1+2j,Nx=Nx,Nt=Nt,T=T,display=False)
    g2 = nwave(1-1j,1+2j,Nx=Nx,Nt=Nt,T=T,display=False)
    
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]

    t = np.linspace(0,T,Nt)[cutoff:]
    
    g1 = g1[cutoff:,:]
    g2 = g2[cutoff:,:]
    
    taper = hann(Nt-cutoff)
    
    
    ####2-d fourier transform?
    r1 = np.fft.fft2(g1)
    r2 = np.fft.fft2(g2)
    plt.figure()
    plt.contour(np.log(np.abs(np.fft.fftshift(r1))))
    plt.figure()
    plt.contour(np.log(np.abs(np.fft.fftshift(r2))))
    
    ## correlation sum
    epsilon = np.logspace(-1,2)
    
    
    D1 = sp.spatial.distance.pdist(g1)
    D2 = sp.spatial.distance.pdist(g2)
    C1 = np.zeros(50)
    C2 = np.zeros(50)
    for i in range(len(epsilon)):
        D3 = D1[D1<epsilon[i]]
        D4 = D2[D2<epsilon[i]]
        C1[i] = D3.size
        C2[i] = D4.size
    
    
    eps1 = epsilon[C1>0]
    C1 = C1[C1>0]
    eps2 = epsilon[C2>0]
    C2 = C2[C2>0]
    
    plt.figure()
    plt.loglog(np.log(epsilon),np.log(C1))
    plt.figure()
    plt.loglog(np.log(epsilon),np.log(C2))
    
    
    return None #modify as needed


def wavediff():
    """
    Question 1.3
    Add input/output as needed

    Discussion: Add discussion here
    
    
    
    
    """
    N = 100
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]
    Nx=256
    Nt=801
    T=200
    
    def fourierDiff(X):
        c = np.fft.fft(X)
        n = np.fft.fftshift(np.linspace(-Nx/2,Nx/2-1,Nx))
        k = 2j*np.pi*n/L
        dx = np.fft.ifft(k*c)
        return dx
    
    
    
    
    import scipy.sparse as sp
    from scipy.sparse import linalg as sl
    def FDDiff(X,alpha = 3/8,a=25/16,b=1/5,c=-1/80):
    
        
        # LHS
        zv = np.ones(N)
        apgv = alpha*zv[1:]
        A = sp.diags([apgv,zv,apgv],[-1,0,1])
        #RHS
        
        zgv = np.zeros(N)
        agv = a*zv[1:]/(2*h)
        bgv = b*zv[2:]/(4*h)
        cgv = c*zv[3:]/(6*h)
        B = sp.diags([-cgv,-bgv,-agv,zgv,agv,bgv,cgv],[-3,-2,-1,0,1,2,3])
        
        b = B*X
        
        return sl.spsolve(A,b)
    
    def FDD(X,alpha = 3/8,a=25/16,b=1/5,c=-1/80):
    
        
        # LHS
        A = np.ones((3,N))
        A = np.array([[alpha,1,alpha]]).T*A
        A[0,0] = 0
        A[0,1] = 3
        A[-1,-1] = 0
        A[-1,-2] = 3
        
       
        #RHS
        
        zgv = np.zeros(N)
        zgv[0] = (-17/6)/h
        zgv[-1] = (17/6)/h
        agv = a*zv[1:]/(2*h)
        agv[0] = (3/2)/h
        bgv = b*zv[2:]/(4*h)
        bgv[0] = (3/2)/h
        cgv = c*zv[3:]/(6*h)
        cgv[0] = (-1/6)/h
        
        magv = -a*zv[1:]/(2*h)
        magv[-1] = (-3/2)/h
        mbgv = -b*zv[2:]/(4*h)
        mbgv[-1] = (-3/2)/h
        mcgv = -c*zv[3:]/(6*h)
        mcgv[-1] = (1/6)/h
        
        B = sp.diags([mcgv,mbgv,magv,zgv,agv,bgv,cgv],[-3,-2,-1,0,1,2,3])
        b = B*X
        
        return solve_banded((1,1),A,b)
    
    
    #### Compare two estimates directly
    
    #### compare time
    
    #### compare grid independence
    
    ##### compare edges.
    
    ##(compare wavenumber analysis)
    
        
    return None #modify as needed

if __name__=='__main__':
    x=None
    #Add code here to call functions above and
    #generate figures you are submitting
