# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 22:44:30 2019

@author: dantr
"""

#from scipy.signal.windows import hann

L = 100
Nx = 256
Nt = 2048
T = 512

g1 = nwave(1-2j,1+2j,Nx=Nx,Nt=Nt,T=T)
g2 = nwave(1-1j,1+2j,Nx=Nx,Nt=Nt,T=T,display=False)


cutoff = int(50/T *(Nt-1))

x = np.linspace(0,L,Nx+1)
x = x[:-1]

t = np.linspace(0,T,Nt)[cutoff:]
Nt = t.size
T = t[-1]-t[0]
#taper = hann(Nt-cutoff)

g1 = g1[cutoff:,:]
g2 = g2[cutoff:,:]


f1, px1 = sp.signal.welch(g1,fs=T/Nt,axis=0)
fr1 = f1*Nt/T
mean_c1 = np.mean(px1,axis=1)
f2, px2 = sp.signal.welch(g2,fs=T/Nt,axis=0)
fr2 = f2*Nt/T
mean_c2 = np.mean(px2,axis=1)

plt.figure()
plt.plot(np.fft.fftshift(fr1),np.fft.fftshift(mean_c1))
plt.plot(np.fft.fftshift(fr2),np.fft.fftshift(mean_c2))
plt.title("Spectral power density estimates - time")
plt.xlabel("Frequency")
plt.legend(["A","B"])

f11, px11 = sp.signal.welch(g1,fs=L/Nx,axis=1)
fr11 = f11*Nx/L
mean_c11 = np.mean(px11,axis=0)
f21, px21 = sp.signal.welch(g2,fs=L/Nx,axis=1)
fr21 = f21*Nx/L
mean_c21 = np.mean(px21,axis=0)

plt.figure()
plt.plot(np.fft.fftshift(fr11),np.fft.fftshift(mean_c11))
plt.plot(np.fft.fftshift(fr21),np.fft.fftshift(mean_c21))
plt.title("Spectral power density estimates - space")
plt.xlabel("Wavenumber")
plt.legend(["A","B"])