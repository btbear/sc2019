# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 17:09:54 2019

@author: dantr
"""

g1 = nwave(1-2j,1+2j,Nx=256,Nt=801,T=200)
g2 = nwave(1-1j,1+2j,Nx=256,Nt=801,T=200,display=False)

L = 100
Nx = 256
Nt = 801
T = 200

cutoff = int(50/T *(Nt-1))

x = np.linspace(0,L,Nx+1)
x = x[:-1]

t = np.linspace(0,T,Nt)[cutoff:]

from scipy.signal.windows import hann

taper = hann(Nt-cutoff)

g1 = g1[cutoff:,:]
g2 = g2[cutoff:,:]

taped_g1 = (g1.T*taper).T
taped_g2 = (g2.T*taper).T

c1 = np.fft.fft(taped_g1,axis=0)/(Nt-cutoff)
mean_c1 = np.mean(c1,axis=1)
c2 = np.fft.fft(taped_g2,axis=0)/(Nt-cutoff)
mean_c2 = np.mean(c2,axis=1)

n = np.arange(-(Nt-cutoff)/2,(Nt-cutoff)/2)
plt.figure()
plt.semilogy(n,np.fft.fftshift(np.abs(mean_c1)**2))
plt.semilogy(n,np.fft.fftshift(np.abs(mean_c2)**2))