# -*- coding: utf-8 -*-
"""
Created on Sun Mar 17 14:10:01 2019

@author: dantr
"""


L = 100
x = np.linspace(0,L,Nx+1)
x = x[:-1]
Nx=256
Nt=1024
T=512
N = Nx
h = L/Nx

testnum = int(100/T *(Nt-1)) - cutoff

testwave = g2[testnum,:]

def fourierDiff(X):
    c = np.fft.fft(X)
    n = np.fft.fftshift(np.linspace(-Nx/2,Nx/2-1,Nx))
    k = 2j*np.pi*n/L
    dx = np.fft.ifft(k*c)
    return dx


def FDDiff(X,h,alpha = 3/8,a=25/16,b=1/5,c=-1/80):

    N = X.size
    # LHS
    zv = np.ones(N)
    apgv = alpha*zv[1:]
    A = sp.diags([apgv,zv,apgv],[-1,0,1])
    #RHS
    
    zgv = np.zeros(N)
    agv = a*zv[1:]/(2*h)
    bgv = b*zv[2:]/(4*h)
    cgv = c*zv[3:]/(6*h)
    B = sp.diags([-cgv,-bgv,-agv,zgv,agv,bgv,cgv],[-3,-2,-1,0,1,2,3])
    
    b = B*X
    
    return sp.linalg.spsolve(A,b)

def FDD(X,h,alpha = 3/8,a=25/16,b=1/5,c=-1/80):

    N = X.size
    # LHS
    A = np.ones((3,N))
    A = np.array([[alpha,1,alpha]]).T*A
    A[0,0] = 0
    A[0,1] = 3
    A[-1,-1] = 0
    A[-1,-2] = 3
    
   
    #RHS
    zv = np.ones(N)
    zgv = np.zeros(N)
    zgv[0] = (-17/6)/h
    zgv[-1] = (17/6)/h
    agv = a*zv[1:]/(2*h)
    agv[0] = (3/2)/h
    bgv = b*zv[2:]/(4*h)
    bgv[0] = (3/2)/h
    cgv = c*zv[3:]/(6*h)
    cgv[0] = (-1/6)/h
    
    magv = -a*zv[1:]/(2*h)
    magv[-1] = (-3/2)/h
    mbgv = -b*zv[2:]/(4*h)
    mbgv[-1] = (-3/2)/h
    mcgv = -c*zv[3:]/(6*h)
    mcgv[-1] = (1/6)/h
    
    B = sp.sparse.diags([mcgv,mbgv,magv,zgv,agv,bgv,cgv],[-3,-2,-1,0,1,2,3])
    b = B*X
    
    return sp.linalg.solve_banded((1,1),A,b)


dif1 = fourierDiff(testwave)
dif2 = FDD(testwave,h)

#### Compare two estimates directly
plt.figure()
plt.plot(x,dif2)
plt.plot(x,dif1)
plt.legend(['FD','Fourier'])
#### compare time

#### compare grid independence

##### compare edges.

##(compare wavenumber analysis)